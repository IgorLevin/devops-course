# Question 1:
'''
You use range in a for loop when you need to iterate on through a fixed length of numbers
'''

# Question 2:
'''
Range with jumps is written creating a range function with three numbers at least and the third numbers is the one that is used to jump
TO go down in a range from high to low numbers. THe third number in the function need to be negative
'''

# Question 3:
'''
You create list from a range by encapsulating a range function in a list function: list(range(num))
'''

# Question 4:
'''
Do-While loop is used when you need iterate at least ones in a loop or needed to iterate overbefore checking the condition
a While loop is used when you need to check the condtion before you iterate inside the loop
'''

# Question 5:
'''
The differnce between a while llop and a do-while is that in the first one you check if the condition and then iterate and the second you go inside the loop and then check the condition
'''

# Question 6:
'''
In python a do-while can emulated by creating an always true while loop (infinite loop) and giving an exit (break) condtion in the ecnd of the loop.
while True:
    do something
    if something is true:
        break
'''

# Question 7:
'''
The differnce between break and continue is when break is used you exit the current loop you are in and in continue you jump to the next iteration.
Its best to use continew when you just need to jump over to the next iteration because of some condtion and not exit the whole loop because of it.
'''

# Question 8:
print('Question 8:')
for num in range(200,2-1,-1):
    print(num, end=' ')
print()

# Question 9:
print('Question 9:')
for num in range(1 - 1,100 + 1,7):
    print(num)
print()

# Question 10:
print('Question 10:')
inp = int(input('Enter a number: '))
sum = 0
while inp >= 0:
    sum += inp
    inp = int(input('Enter a number: '))
print(sum)
print()

# Question 11:
print('Question 11:')
inp = int(input('Enter a number: '))
sum = 1
for num in range(0,inp):
    sum *= num+1
print(sum)
print()

# Question 12:
print('Question 12:')
lucky_numbers = []
while len(lucky_numbers) < 5:
    inp = int(input('Enter a random number between 2 and 49: '))
    if inp not in lucky_numbers and 2 <= inp <= 49:
        lucky_numbers.append(inp)

num_of_tries = 0
numbers_guessed = []
while len(lucky_numbers) > 0:
    
    num_of_tries += 1
    if num_of_tries > 20:
        print('Resetting tries and lucky numbers list')
        num_of_tries = 0
        numbers_guessed = []
        lucky_numbers = []
        while len(lucky_numbers) < 5:
            inp = int(input('Enter a random number between 2 and 49: '))
            if inp not in lucky_numbers and 2 <= inp <= 49:
                lucky_numbers.append(inp)
        continue
    print(f'Try number: {num_of_tries}')

    inp = int(input('Guess a number: '))
    if inp in numbers_guessed:
        print('Number was guessed already, exiting...')
        break
    else:
        numbers_guessed.append(inp)

    if 2 > inp > 49:
        print('Number not in range')
        continue

    if inp in lucky_numbers:
        print('You guessed correctly')
        lucky_numbers.remove(inp)
    else:
        print('Wrong guess, try again')

print(f'Number of tries: {num_of_tries}')
