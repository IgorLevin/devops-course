import math

# Question 1
print('Question 1:')
print('''A function is a block of reusable code that you can call by its name 
and use it when you need it without writing it again or using a loop to use the code multiple times.''')
print()

# Question 2
print('Question 2:')
print('''A function is declared by using the word "def", then there is the name of the function, after that it is ending with parenthesis and a colon''')
print()

# Question 3
print('Question 3:')
print('''You can add parameters to a funtion by writing them inside the function's parenthesis, separated by commas.
The function of parameters in a python function to pass differnet values every time you want to get a different output.
To give default values to a function's parameters you need to assign them during the function declaration in the function parenthesis with a sign like a normal variable assignment.''')
print()

# Question 4
print('Question 4:')
print('''To pass a value to a specific parameter in python you need to pass the value of the parameter to the function when you call thw parameter's name and an assigment sign.
Like this: myFunc(myParam = "newValue")''')
print()

# Question 5
print('Question 5:')
print('''It's preferably that the function will return the value if you need to store its output in a variable and use it later or use in a speific way outside of the function.
A function in python returns a value by using the saved python word "return" in the end of the function or when you need the function to end and return the output.''')
print()

# Question 6
print('Question 6:')
print('''An empty funcction in python is a function that does not return a value.
To use an empty function you have to use the saved python word "pass", so that the whole function will not return a value or a section of it if using conditions.''')
print()

# Question 7
print('Question 7:')
print('''To create random numbers in python you have to import the "random" library and call the random class, 
followed by a dot and the "randint" function, 
ending with parenthesis inside the two numbers that show the begging and the end of the range of the random numbers.''')
print()

# Question 8
print('Question 8:')
def get_circle_circumference(radius):
    return 2 * math.pi * radius

print(f'The circumference of a circe with radius of {5} is {get_circle_circumference(5)}')
print()

# Question 9
print('Question 9:')
def add(x = 0, y = 0):
    return x + y
def sub(x = 0, y = 0):
    return x - y
def mul(x = 0, y = 0):
    return x * y
def div(x = 0, y = 0):
    return x / y

num_one = int(input('Enter first number: '))
num_two = int(input('Enter second number: '))
print(f'Add function: {add(num_one, num_two)}')
print(f'Sub function: {sub(num_one, num_two)}')
print(f'Mul function: {mul(num_one, num_two)}')
print(f'Div function: {div(num_one, num_two)}')
print()

# Question 10
print('Question 10:')
def getInRange(min, max):
    while True:
        num = int(input('Enter a number: '))
        if min < num < max:
            return num

number = getInRange(10,100)
print(f'{number} is in range')
print()

# Question 11
print('Question 11:')
def get_lower_number(num_one, num_two, num_three):
    if num_one < num_two:
        if num_one < num_three:
            return num_one
        else:
            return num_three
    elif num_two < num_three:
        return num_two
    else:
        return num_three

print(f'Lowest number is: {get_lower_number(5, 23, 1)}')
print()