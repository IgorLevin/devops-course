# Question 1:
'''
A loop is used when you need to do the same operation over and over many times like going over a list of numbers.
'''

# Question 2:
'''
Loop syntax:
for temp_varaible in list_of_values:
    do something (mostly with temp_varaible)
exit loop
'''

# Question 3:
print('Question 3:')
num_list = [1, 5, 7, 8, 100]
for num in num_list[:len(num_list)//2]:
    print(num)
print()

# Question 4:
print('Question 4:')
words_list = ['hello', 'python', 'pen', 'coding of world']
for word in words_list:
    print(word.upper())
print()

# Question 5:
print('Question 5:')
words_list = ['hello', 'python', 'pen', 'coding of world']
for word in words_list:
    if len(word) < 4:
        break
    else:
        print(word.upper())
print()

# Question 6:
print('Question 6:')
full_name = 'Igor Levin'
print(full_name[-5:])
print(full_name[:len(full_name)//3])
print(full_name.count('a'))
print(full_name.count('b') > 0)
full_name_list = full_name.split()
full_name_list.reverse()
full_name_list[0] = full_name_list[0].upper()
# getting the middle char of the name if its length is an odd number or two middle chars if its length is an even number
char_rm = full_name_list[1][len(full_name_list[1])//2] if len(full_name_list[1]) % 2 != 0 else full_name_list[1][len(full_name_list[1])//2-1:len(full_name_list[1])//2+1]
full_name_list[1] = full_name_list[1].replace(char_rm,'')
print(full_name_list)
print()

# Question 7:
print('Question 7:')
string = "Hello World!"
print(f"First index: {string.index('o')} Last index: {string.rindex('o')}")
print(string[:string.index('o')+1])
print(string[string.rindex('o'):])
print()

# Question 8:
print('Question 8:')
print('Hello World'.replace('o',''))
print()

# Question 9:
print('Question 9:')
num_list = [8, 1000, -3, 2, 5]
print(sum(num_list))
print(max(num_list))
print(min(num_list))
print(sum(num_list) / len(num_list))
num_list.pop(len(num_list)//2)
num_list.sort()
# print one list with all of its values 5 times
print(num_list * 5)
# using list comprehension to print a new list containing the original list 5 times
print([num_list for num in range(5)])
num_list.pop(0)
# using list comprehension to get only the numbers less than the average of the list
num_sub_list = [num for num in num_list if num < sum(num_list) / len(num_list)]
print(num_sub_list)
print()

# Question 10:
print('Question 10:')
num_list = [1, 5, 7, 8, 100]
largest_num = num_list[0]
for num in num_list:
    if largest_num < num:
        largest_num = num
print(largest_num)
print()

# Question 11:
print('Question 11:')
num_lists = [[4, 8, 200], [4, 3000, -1], [5, 87, 12]]
smallest_num = num_lists[0][0]
for num_list in num_lists:
    for num in num_list:
        if smallest_num > num:
            smallest_num = num
print(smallest_num)
print()
