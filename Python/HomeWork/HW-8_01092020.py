# Question 1:
print('Question 1:')

def get_args_as_list(*args):
    return list(args)
print(get_args_as_list(1,2,3,4,5,6))
print()

# Question 2:
print('Question 2:')

def get_args_as_dictionary(a,b,*args):
    args_dictionay = dict()
    args_dictionay['positional_arguments'] = [a,b]
    args_dictionay['*args'] = list(args)
    return args_dictionay
print(get_args_as_dictionary(1,2,3,4,5,6))
print()

# Question 3:
print('Question 3:')

def get_tuple_from_list(list_one):
    return tuple(list_one)
print(get_tuple_from_list([1,2,3,4]))
print()

# Question 4:
print('Question 4:')

def counter_tuples(*args):
    count_dict = dict()
    for arg in args:
        if not arg in count_dict.keys():
            count_dict[arg] = args.count(arg)
    return count_dict
print(counter_tuples('a','b','a',1,3,1))
print()

# Question 5:
print('Question 5:')

def discovery(param):
    if type(param) is list:
        print('list can be modified in any way')
    elif type(param) is tuple:
        print('tuple cannot be modified in any way')
    elif type(param) is dict:
        print('dictionary can be modified in any way')
    elif type(param) is set:
        print('set can be modified in any way, but duplicated values will be ignored!')
    else:
        print('Not a collection type')
discovery([1,2,3])
discovery((1,2,3))
discovery({ 'a' : 2, 'b' : 1, 1 : 2, 3 : 1 })
discovery({1,2,3})
discovery(1)
print()