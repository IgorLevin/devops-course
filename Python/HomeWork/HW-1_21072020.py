#Question 1:
print('Question 1:')
input_str = input('Enter a string: ')
print(f'input returns a {type(input_str)} and its value is: {input_str}')
input()

#Question 2:
print('Question 2:')
input_int = int(input('Enter an integer: '))
input_float = float(input('Enter a float: '))
print(f'Integer input returns a {type(input_int)} and its value is: {input_int}')
print(f'Float input returns a {type(input_float)} and its value is: {input_float}')
input()

#Question 3:
print('Question 3:')
str_split = 'This is for split question'
print(f'Split return a list of values: {str_split.split()}')
print(f'Split returns type: {type(str_split.split())}')
print(f"Split using a comma instead of a space: {str_split.replace(' ',',').split(',')}")
input()

#Question 4:
print('Question 4:')
str_format = 'This is for format question'
print("Format lets us use variables like this: {}".format(str_format))
print("Instead of using it in the following: " + str_format + " because if we hav a lot varaibles its gets messy")
input()

#Question 5:
print('Question 5:')
list_question = [1, 2, 3, 4]
print(f'In returns {1 in list_question} if it finds a value in a list and {5 in list_question} if not.')
input()

#Question 6:
print('Question 6:')
print('''Conditions writen using if, elif and else in the following way:
    if a == 1:
        return when if is true
    elif a == 2:
        return when elif is true and when if is false and the elif before are false
    else:
        returns only when if and all elif are false
''')
print('''The differnece between elif and else:
    elif have a condition like if and we can have as many as we want and else is excuted only when if and all elif return false:
        if a == 1:
            return a
        elif a == 2:
            return a
        elif a == 3:
            return a
        else:
            retun and            
''')
input()

#Question 7:
print('Question 7:')
print('''Python know that the code is for if by using space after the if line. 
If the next lines of code are with a space or a tab, python will consider them as part of the if statement.
    if true:
        this is part of if statement
        thsi is also part of if statement
    this is not part of if statement
''')
input()

#Question 8:
print('Question 8:')
num_one = int(input('Enter first number: '))
num_two = int(input('Enter second number: '))
if num_one > num_two:
    print(f'First number is bigger: {num_one}')
elif num_one < num_two:
    print(f'Second number is bigger: {num_two}')
else:
    print(f'Numbers are equal: {num_one} {num_two}')
input()

#Question 9:
print('Question 9:')
num_one = int(input('Enter first number: '))
num_two = int(input('Enter second number: '))
num_three = int(input('Enter Third number: '))
if num_one > num_two:
    if num_one > num_three:
        print(f'First number is biggest: {num_one}')
    else:
        print(f'Third number is biggest: {num_three}')
else:
    if num_two > num_three:
        print(f'Second number is biggest: {num_two}')
    else:
        print(f'Third number is biggest: {num_three}')
input()

#Question 10 + Question 11:
print('Question 10 + Question 11:')
def check(num_one, num_two, operand, calc_operand, calculation):
    if calc_operand == calculation:
        print(f'True\n{num_one} {operand} {num_two} == {calculation}')
    else:
        print(f'False\n{num_one} {operand} {num_two} != {calculation}')

formula = input('Enter formula: ')
formula_list = formula.split()
if len(formula_list) != 5:
    print(f'Bad formula')
elif not formula_list[1] in ['+','-','*','/'] or formula_list[3] != '=':
    print(f'Bad formula')
else:
    num_one = float(formula_list[0])
    num_two = float(formula_list[2])
    operand = formula_list[1]
    calculation = float(formula_list[4])
    if operand == '+':
        check(num_one, num_two, operand, float(num_one+num_two), calculation)
    elif operand == '-':
        check(num_one, num_two, operand, float(num_one-num_two), calculation)
    elif operand == '*':
        check(num_one, num_two, operand, float(num_one*num_two), calculation)
    elif operand == '/':
        check(num_one, num_two, operand, float(num_one/num_two), calculation)
    else:
        print('Error')
