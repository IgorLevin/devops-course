# Question 1
print('Question 1:')
print('''Slice on a string is used to slice/cut a string and get an output based on the parameters you gave it, from where to start and where to finish''')
print()

# Question 2
print('Question 2:')
print('''Concatenation on a string is used to merge strings together or strings with varaibles to get one output''')
print()

# Question 3
print('Question 3:')
print('''List Comprehension is a way to create a new list using iterables.
We use list comprehension when we want to create a new from some iterable or anothe list
The advantage of list comprehension is that its a short one liner code that retruns you a list.''')
print()

# Question 4
print('Question 4:')
print('''A Dictionary in python is a type of collection of objects that are ordered in a key and value format. It means that when you want a value from the dictionary you need to specifiy the relavant key.
We use a Dictionary when we need to store values in a collection in an unordered way so we can get those values by specifyinc unique keys get them.
It's better to use a dictionary when we have an unordered collection so we can't know excactly when every value is by just iterating it and only by its key.''')
print()

# Question 5
print('Question 5:')
print('''A key in a dictionary can be a string or a number.
The value of a dictionary can be of any type.''')
print()

# Question 6
print('Question 6:')
print('''An entry in a dictionary is the way you get the value of an object in a dictionay by its key''')
print()

# Question 7
print('Question 7:')
print('''THe format of a dictionary is JSON''')
print()

# Question 8
print('Question 8:')
print('''Create an empty dictionary: dict_var = {}
Add to a dictionary: dict_var[new_key] = value.
To replace an entry or a value in a dictionary: dict_vat[existing_key] = new_value.
To remove from a dictionary: dict_var.pop(existing_key) or del dict_var[existing_key].
To remove the whole dictionary: del dict_var
''')
print()

# Question 9 + 10
print('Question 9 + 10:')
print('''To get all the keys in a dictionary we use the dictionary inner function keys(): dict_var.keys()
To get all the values in a dictionary we use the dictionary inner function values(): dict_var.values()''')
print()

# Question 11
print('Question 11:')
string = 'I love to eat ice cream in the beach'
[print(word.upper()) for word in string.split()]
[print(char[0]) for char in string.split()]
[print(char[2]) for char in string.split() if len(char) > 2]
[print(len(num)) for num in string.split()]
print()

# Question 12
print('Question 12:')
[print(f'{10 ** num} {num}') for num in range(1,10)]
print()

# Question 13
print('Question 13:')
def tryGetValue(d, k):
    if k in d.keys():
        return d[k]
    else:
        return None

dictionary = {1:'a',2:'b',3:'c'}
print(f'The value of {1} is {tryGetValue(dictionary,1)}')
print()

# Question 14
# In this question I sorted not only the keys but the whole dictionary by keys
print('Question 14:')
def getSortedDict(d):
    sorted_keys = sorted(list(d.keys()))
    sorted_dict = {}
    for n in range(len(sorted_keys)):
        sorted_dict[sorted_keys[n]] = d[sorted_keys[n]]
    return sorted_dict

dictionary = {3:'c',2:'b',1:'a'}
print(f'The sorted dictonary: {getSortedDict(dictionary)}')
print()

# Question 15
print('Question 15:')
def mergeDictionary(d1,d2):
    merged_dict = {}
    for n in range(len(d1) + len(d2)):
        if n < len(d1):
            merged_dict[list(d1.keys())[n]] = d1[list(d1.keys())[n]]
        else:
            merged_dict[list(d2.keys())[len(d2)-n]] = d2[list(d2.keys())[len(d2)-n]]
    return merged_dict

dictionary_one = {1:'a',2:'b'}
dictionary_two = {3:'c',4:'d'}
print(f'The merged dictionary: {mergeDictionary(dictionary_one, dictionary_two)}')
print()

# Question 16:
print('Question 16:')
dict_of_people = {}
while True:
    id_number = int(input('Enter an ID number: '))
    if id_number == -1:
        break
    elif id_number in dict_of_people.keys():
        print(f'The person with ID {id_number} and name {dict_of_people[id_number]} already in dictonary')
    else:
        name = input('Enter a name: ')
        dict_of_people[id_number] = name

print(f'The added people: {dict_of_people}')
print()