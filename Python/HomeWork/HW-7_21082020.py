# Question 1:
'''
A varaible that is declared inside a function is considered a local variable.
'''

# Question 2:
'''
Yes, you can call a read only global variable
'''

# Question 3:
'''
The value of the varaible will change only inside the function scope and keep itd value outside the function scope.
'''

# Question 4:
'''
It's better that the varaibles inside a function will not be global because you may need only to change the variable inside the function or use it with it's original
value at another function every time or call it in different ways. If you use global every time you call the function with the global variable you channge it value permanently.
'''

# Question 5:
'''
The meaning of global is to create a variable inside the function scope that you can use or change outside the function scope in a "global way" in your runtime.
'''

# Question 6:
'''
You can return two values using a collection the retrun statement (list, set or dictionary) or just retrun the values seperated by commas.
'''

# Question 7:
'''
Set is unique in the way that its retrun an unordered collection of only unique values
'''

# Question 8:
'''
The command "open" is used in python to make open to reading and editing.
'''

# Question 9:
'''
The main options of opening files are:
 - Read (r) = Opens a file for reading the contents of a file
 - Write (w) = Opens a file for writing data to a file, overwriting if file exist and creating a file if does not exist
 - Append (a) = Opens a file for appending data to a file without deleting the previous data and creating a file if does not exist
 - Read + Write (r+) = Opens a file for reading and writing data to a file.
 - Write + Read (w+) = Opens a file for writing and reading data from and to a file, overwriting if file exist and creating a file if does not exist
 - Append + Read (a+) = Opens a file for appending and reading data from and to a file, without deleting the previous data and creating a file if does not exist
'''

# Question 10:
'''
It's important to close a file when finishing working with it becuase if it stays open the program can still write to it and manage it evne when we don't need to.
'''

# Question 11:
'''
To open and close file for sure we can use the 'with' command with parentheses after that. In this command the file opens inside the scope of the 'with' command and closed
when we go outside of it. 
'''

# Question 12:
'''
The 'seek' command changes the current position a differnet position inside of a file based on the parameter it gets.
The 'tell' command gets us out current position in a file.
'''

# Question 13:
print('Question 13:')
list_of_num = []
set_of_num = {}
counter = 0
while True:
    inp = int(input('Enter a number: '))
    if inp == -1:
        break
    counter += 1
    list_of_num.append(inp)
    set_of_num = set(list_of_num)
duplicated_nums = len([num for num in set_of_num if list_of_num.count(num) > 1])
print(f'Number of numbers added: {counter}\nNumber of numbers (without duplications): {len(set_of_num)}\nNumber of numbers with duplications: {duplicated_nums}')

# Question 14:
print('Question 14:')
with open('Q14-File.txt','a+') as f:
    while True:
        inp = input('Enter a word: ')
        if inp.lower() == 'exit':
            break
        f.write(f'{inp}\n')
    f.seek(0)
    [print(line.strip()) for line in f.readlines()]

# Question 15:
print('Question 15:')
# Creating a default file for the question
with open('Q15_File.txt','w') as f:
    f.writelines('1\n2\n3\n4\n5\n')

def add_sum(file_name):
    with open(file_name,'r+') as f:
        sum_of_num = 0
        sum_of_num = sum([int(num.strip()) for num in f.readlines()])
        print(f'Adding sum of numbers to file')
        f.write(str(sum_of_num))
add_sum('Q15_File.txt')

# Question 16:
print('Question 16:')
# Creating a default file for the question
with open('Q16_File.txt','w') as f:
    f.writelines('Hello\nWorld\nThis\nIs\nPython\n')

def check_word(file_name,word):
    with open(file_name,'r') as f:
        list_of_words = [word.strip() for word in f.readlines()]
        if word in list_of_words:
            return True
        else:
            return False
print(check_word('Q16_File.txt','Hello'))
print(check_word('Q16_File.txt','Python'))
print(check_word('Q16_File.txt','NoWord'))

# Question 17:
print('Question 17:')
# Creating a default file for the question
with open('Q17_Source_File.txt','w') as f:
    f.writelines('Hello\nWorld\nThis\nIs\nPython\n')

def reverse_output(source_file, destination_file):
    with open(source_file, 'r') as sf, open(destination_file, 'w') as df:
        source_content = [word.strip() for word in sf.readlines()]
        destination_content = [word[::-1] for word in source_content[::-1]]
        print(f'Writing to a file the following: {destination_content}')
        df.write('\n'.join(destination_content))
reverse_output('Q17_Source_File.txt','Q17_Destination_File.txt')
