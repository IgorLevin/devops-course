import random
num = random.randint(1,100)
num_of_guess = 0

while True:
    inp = int(input('Guess a number: '))

    if num_of_guess == 5:
        print('Number of guesses excedded\nStatring over...')
        num = random.randint(1,100)
        num_of_guess = 0
        continue

    if inp > num:
        print('Guess lower')
        num_of_guess += 1
    elif inp < num:
        print('Guess higher')
        num_of_guess += 1
    else:
        print('Correct')
        print(f'Number of guesses: {num_of_guess}')
        break