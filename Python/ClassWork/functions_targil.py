# 1
# write function mySqr with parameter x
# and return x ** 2
def mySqr(x):
    return x ** 2
# 2
# write a function myMul with parameters x, y
# and returns x + y
def myMul(x,y):
    return x + y
# 3
# write a function myFactorial with parameter x
# this function calculate the x! factorial (1*2*3*4...*x)
# and return this factorial
def myFactorial(x):
    factorial = 1
    for num in range(1,x+1):
        factorial += num
    return factorial
# 4
# write a function myCheckEven with parameter x
# return boolean True if x is even (2, 4, 10, ...)
# return False if it is odd  (1, 3, 5 ...)
def myCheckEven(x):
    if x % 2 == 0:
        return True
    else:
        return False
# 5
# write a function myAverage with parameter l1 (=list)
# return the average of this list
def myAverage(l1):
    return sum(l1) / len(l1)
# 6
# write a function myConcat with parameters s1, s2 (=string)
# return s1 + " " + s2
def myConcat(s1, s2):
    return s1 + " " + s2

def main():
    print(f'Question 1: {mySqr(4)}')
    print(f'Question 2: {myMul(3,4)}')
    print(f'Question 3: {myFactorial(4)}')
    print(f'Question 4: {myCheckEven(4)} {myCheckEven(5)}')
    print(f'Question 5: {myAverage([1,2,3,4,5,6])}')
    print(f'Question 6: {myConcat("Hello","World!")}')

main()

