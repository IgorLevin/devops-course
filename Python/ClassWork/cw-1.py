# Question 1:
print('Question 1:')
inp = int(input('Enter a number: '))
num = 1
while num < inp:
    if inp % num == 0:
        print(num)
    num += 1
print()

# Question 2:
print('Question 2:')
num = inp_sum = avg = inp = 0
while True:
    num += 1
    if inp < 0:
        print('Thank you. Goodbye.')
        break
    elif num == 1:
        inp = int(input(f'Please enter #{num}: '))
        inp_sum = inp
        avg = inp
    else:
        inp = int(input(f'Please enter #{num} (avg={avg}. sum={inp_sum}): '))
        inp_sum += inp
        avg = inp_sum // num
print()

# Question 3:
print('Question 3:')
word_list = []
while True:
    word = input(f'Please enter a word: ')
    #if word not in word_list:
    #if word_list.count(word) != 2:
    temp_word_list = [word.lower() for word in word_list]
    if temp_word_list.count(word.lower()) != 2:
        word_list.append(word)
    else:
        print(f'You entered the word {word} three times. Goodbye...')
        break
print()

# Question 4:
print('Question 4:')
list_one = [3,5]
list_two = [1000,5,29,-5]
num = list_one_points = list_two_points = 0
def bigger_list(list_one, list_two, list_len, num, list_one_points, list_two_points):
    while num < list_len:
        if list_one[num] > list_two[num]:
            list_one_points += 1
        elif list_two[num] > list_one[num]:
            list_two_points += 1
        num += 1
    print(f'List {1 if list_one_points > list_two_points else 2} is bigger')

if len(list_one) < len(list_two):
    bigger_list(list_one, list_two, len(list_one), num, list_one_points, list_two_points)
elif len(list_two) > len(list_one):
    bigger_list(list_one, list_two, len(list_two), num, list_one_points, list_two_points)
else:
    bigger_list(list_one, list_two, len(list_one), num, list_one_points, list_two_points)
print()
