
def statistics(k, *args, **kwargs):
    print(f'The type of k: {type(k)} The value of k: {k}')
    if len(args) != 0:
        rep = True if len(set(args)) != len(args) else False
        print(f'Length of args: {len(args)} Values of args: {args} Repitition in args: {rep}')
    else:
        print('args is empty')
    if len(kwargs) != 0:
        kwargs_dict = {k:v for k,v in kwargs.items()}
        k_in_dict = True if k in kwargs_dict.keys() else False
        kwargs_keys_list = [k for k in kwargs_dict.keys()]
        kwargs_values_list = [v for v in kwargs_dict.values()]
        print(f'''Length of kwargs: {len(kwargs)} Kwargs keys: {kwargs_dict.keys()} Kwargs values: {kwargs_dict.values()} Kwargs items: {kwargs_dict.items()} \nk in kwargs keys: {k_in_dict}, kwargs list of keys: {kwargs_keys_list} kwargs list of values: {kwargs_values_list}''')
    else:
        print('kwargs is empty')

statistics('a',1,2,2,3,a=1,b=2,c=3)
