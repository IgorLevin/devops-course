population = {'ISRAEL':9000000,'RUSSIA':146000000,'USA':328000000}

while True:
    print('''Choose an Option:
    1. Get country population
    2. Add country and population to dictionary
    3. Remove country from dictionary
    4. Exit
    ''')
    user_input = input('Enter an option: ')

    if user_input == '1':
        country_input = input('Enter country name: ')
        if country_input.upper() in population.keys():
            print(f'The population of {country_input} in 2020 is: {population[country_input.upper()]}')
        else:
            print(f'Country {country_input} not in dictionary')

    elif user_input == '2':
        country_input = input('Enter country name: ')
        if type(country_input) is str:
            country_population = int(input(f'Enter {country_input} population: '))
            if country_population > 0:
                population[country_input.upper()] = country_population
            else:
                print(f'Country population can\'t be lower than 0')
        else:
            print(f'Country\'s name is invalid')

    elif user_input == '3':
        country_input = input('Enter country name: ')
        if country_input.upper() in population.keys():
            del population[country_input.upper()]
        else:
            print(f'Country {country_input} not in dictionary')
    
    elif user_input == '4':
        'Exiting...'
        break

    else:
        continue


def tryAddValue(d, k, v):
    if k in d.keys():
        print(f'THe value of key {k} is {d[k]}')
    else:
        print(f'The key {k} is not in dictionary, adding it')
        d[k] = v

dictionary = {}
while True:
    key_input = input('Enter key: ')
    value_input = input('Enter value: ')
    tryAddValue(dictionary, key_input, value_input)

def printDictionary(d):
    for k,v in d.items():
        print(f'key {k}: value {v}')

dictionary = {1:'a',2:'b',3:'c'}
printDictionary(dictionary)