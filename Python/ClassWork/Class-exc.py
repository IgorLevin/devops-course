class Dog:
    def __init__(self, name, height, color, brand):
        print("Dog was created")
        self.name = name
        self.height = height
        self.color = color 
        self.brand = brand

    def bark(self):
        print('Barking')
    def jump(self):
        print('Jumping')
    def play(self):
        print('Playing')
    def eat(self):
        print('Eating')

rexi = Dog(name='rexi', height=1.5, color='red', brand='wolfdog')
rexi.bark()
rexi.jump()
rexi.play()
rexi.eat()
print(rexi.__repr__)