import random
import time

# Function to print the gaming board as it updating every turn
def print_board(game_board):
    for row in range(len(game_board)):
        for cell in range(len(game_board)):
            if cell == 0:
                print('|', end=" ")
            print(f'{game_board[row][cell]} |', end=" ")
        print()
    print()

# Fucntion to change the gaming board to differnt sizes
def set_board_size(size = 0):
    # If entered a number larger than two as a parameter in to the function change the board by this number
    if size > 2:
        size = [['_' for cell in range(size)] for row in range(size)]
        return size
    # If no parameter was used when calling the function ask the player to choose a number greater than two for the gaming board
    else:
        while True:
                size = input('Enter the size of the game board: ')
                if size.isdigit():
                    size = int(size)

                    if size > 2:
                        size = [['_' for cell in range(size)] for row in range(size)]
                        return size
                    else:
                        print('The game board size have to be larger than 2')
                else:
                    print('The game board size have to be a valid number')
        
# Function to set the player's choice of cell every turn by their choices
def set_player_cell(game_board, player, player_type):
    row_num = column_num = None
    num_of_options = list(range((len(game_board))))
    
    while True:
        # If a player is a human ask him to choose a row and column tu update the gaming board
        if player_type == 'Human':
            row_num = input(f'Player {player}: Enter a row number: ')
            column_num = input(f'Player {player}: Enter a column number: ')
            
            # Checking if the player choice is a number
            if row_num.isdigit() and column_num.isdigit():
                row_num = int(row_num)-1
                column_num = int(column_num)-1

                # Checking if the numbers are in the range of the game board
                if row_num not in num_of_options or column_num not in num_of_options:
                    print('Wrong row or column number. Please try again')
                # Checking if the cell chosen by the player is not in use already
                elif game_board[row_num][column_num] != '_':
                    print(f'Cell already been used by player {game_board[row_num][column_num]}. Please try again')
                else:
                    # Return player choice
                    return [row_num, column_num]
            else:
                print('Wrong type was entered, only numbers can be enterd. Please try again')

        # If a player is a computer, create random numbers to update the gaming board every turn
        elif player_type == 'Computer':
                row_num = random.randint(0, len(game_board)-1)
                column_num = random.randint(0, len(game_board)-1)
                if row_num in num_of_options and column_num in num_of_options and game_board[row_num][column_num] == '_':
                    return [row_num, column_num]

# Function to check if a player has won by having all of the cells in a row with his choices or can win in next turn
def check_rows(game_board, player):
    num_of_rows_columns = len(game_board)
    player_cells = 0
    player_cells_list = [0,0]

    # Looping over all the rows in a game board
    for row in range(num_of_rows_columns):
        if player_cells < num_of_rows_columns:
            # Looping over all the cells in a row
            for cell in range(len(game_board[row])):
                # Checking if a cell containg a previous player choice
                if game_board[row][cell] != player:
                    # Checking if a player can win in the next turn
                    if player_cells == num_of_rows_columns-1 and cell < num_of_rows_columns:
                        if game_board[row][cell] == '_':
                            player_cells_list[1] = [row+1, cell+1]
                    player_cells = 0
                    break
                else:
                    player_cells += 1
        else:
            break
            
    player_cells_list[0] = player_cells
    # Return a list of player cells and a player next turn move for winning
    return player_cells_list

# Function to check if a player has won by having all of the cells in a column with his choices or can win in next turn
def check_columns(game_board, player):
    num_of_rows_columns = len(game_board)
    cell = player_cells = 0
    player_cells_list = [0,0]

    # Looping over all cells in a column
    while cell < num_of_rows_columns:
        # Looping through the cell in a row that match the cell above
        for row in range(num_of_rows_columns):
            if game_board[row][cell] == player:
                player_cells += 1
            else:
                if game_board[row] == 0:
                    continue
                else:
                    # Checking if a player can win in the next turn
                    if player_cells == num_of_rows_columns-1 and row < num_of_rows_columns:
                        if game_board[row][cell] == '_':
                            player_cells_list[1] = [row+1, cell+1]
                    break
        if player_cells != num_of_rows_columns:
            cell += 1
            player_cells = 0
        else:
            break
    
    player_cells_list[0] = player_cells
    # Return a list of player cells and a player next turn move for winning
    return player_cells_list

# Function to check if a player has won by having all of the cells in a diagonal with his choices or can win in next turn
def check_diagonals(game_board, player, direction, cell):
    num_of_rows_columns = len(game_board)
    player_cells = 0
    player_cells_list = [0,0]

    # Looping over all the rowsin a game_board
    for row in range(num_of_rows_columns):
        # Making the loop go from left to right in diagonal
        if direction == 'left_to_right':
            cell = row
        # Making the loop go from right to left in diagonal
        elif direction == 'right_to_left':
            cell -= 1
        if game_board[row][cell] == player:
            player_cells += 1
        else:
            # Checking if a player can win in the next turn
            if player_cells == num_of_rows_columns-1 and cell < num_of_rows_columns:
                if game_board[row][cell] == '_':
                    player_cells_list[1] = [row+1, cell+1]
            break
    
    player_cells_list[0] = player_cells
    # Return a list of player cells and a player next turn move for winning
    return player_cells_list


def check_winner(game_board, player, check_next_turn = False):
    num_of_rows_columns = len(game_board)
    rows = check_rows(game_board, player)
    columns = check_columns(game_board, player)
    diagonals_left_to_right = check_diagonals(game_board, player, 'left_to_right', num_of_rows_columns)
    diagonals_right_to_left = check_diagonals(game_board, player, 'right_to_left', num_of_rows_columns)

    if check_next_turn == False:
        # Checking if a player has won by testing the retrun vaules of all the previous "check" functions
        if (rows[0] == num_of_rows_columns) or (columns[0] == num_of_rows_columns) or (
        diagonals_left_to_right[0] == num_of_rows_columns) or (diagonals_right_to_left[0] == num_of_rows_columns):
            return True
    else:
        # Checkking if a player has a next turn win move in any of the avaliable winning option in the game
        if rows[1] != 0:
            return rows[1]
        elif columns[1] != 0:
            return columns[1]
        elif diagonals_left_to_right[1] != 0:
            return diagonals_left_to_right[1]
        elif diagonals_right_to_left[1] != 0:
            return diagonals_right_to_left[1]
        else:
            return False

    return False

# Function to run to start the program
def main():
    game_board = None
    while True:
        # Printing the main menu with all of the options players has at the start of a game or at a restat of it and asking for them to choose how to play
        print('''Choose an option:
        1. Normal two player game
        2. Player against computer
        3. Computer against himself
        4. Change default game board size
        q. Quit game
        ''')
        player_input = input('Enter your choice: ')
        player_X = player_O = None

        # If choice is '1' a two player game is chosen
        if player_input == '1':
            player_X = player_O = 'Human'

        # If choice is '2' a player against computer game is chosen and the player choose what to be ('X' or 'O')
        elif player_input == '2': 
            while player_input != "X" and player_input != "O":
                player_input = input('Choose X or O for player assigment: ')
                if player_input == 'X':
                    player_X = 'Human'
                    player_O = 'Computer'
                elif player_input == 'O':
                    player_X = 'Computer'
                    player_O = 'Human'
                else:
                    print('No valid option was entered. Please choose again')

        # If choice is '3' a game between the computer and himself start with random choices 
        elif player_input == '3':
            player_X = player_O = 'Computer'

        # If choice is '4' the players can chage the default game board size
        elif player_input == '4':
            game_board = set_board_size()
            print('New game board size:')
            print_board(game_board)
            continue
        
        # If choice is 'q' the game and program ends
        elif player_input.lower() == 'q':
            print('Exiting game...\nGoodbye')
            break
        
        # If non of the previous option were chosen starts over
        else:
            print('No valid option was entered. Please choose again')
            continue
        
        # Starting game at default 3X3 size if option '4' was not chosen
        if game_board == None:
            game_board = set_board_size(3)
        counter = 0
        turn = 0
        num_of_cells = len(game_board) ** 2

        # Starting the loop for every new turn of the game or the restart of it`
        while True:
            game_ended = False
            turn += 1
            print(f'Turn {turn}:')

            # Player X turn in the game
            print('Player X turn: ')
            counter += 1
            player = 'X'
            # Saving player choice in to a varaible
            row_column = set_player_cell(game_board, player, player_X)
            # Setting player choice in the game board
            game_board[row_column[0]][row_column[1]] = 'X'
            # Printing the updated gaming board
            print_board(game_board)
            time.sleep(1)
            # Cheking if Player X has won. If so printing it and ending the current game
            game_ended = check_winner(game_board, 'X')
            if game_ended == True:
                print('Player X has won')
            
            # Player O turn in the game
            # Checking if player O needs to play by if the board not full or player X has won already
            if counter < num_of_cells and game_ended == False:
                print('Player O turn: ')
                counter += 1
                player = 'O'
                # Saving player choice in to a varaible
                row_column = set_player_cell(game_board, player, player_O)
                # Printing the updated gaming board
                game_board[row_column[0]][row_column[1]] = 'O'
                # Printing the updated gaming board
                print_board(game_board)
                time.sleep(1)
                # Cheking if Player O has won. If so printing it and ending the current game
                game_ended = check_winner(game_board, 'O')
                if game_ended == True:
                    print('Player O has won')

            # Checking if there are no more avaliable cells to choose for the players
            # If so ending the current game and printing a tie in game
            if counter == num_of_cells and game_ended == False:
                print('No more avaliable cells.\nIt\'s a tie')
                game_ended = True
            
            # If game not ended giving the players a choice how to continue if at least one on is a human and not computer
            if (player_X == 'Human' or player_O == 'Human') and game_ended == False:
                print("""Press 'y' to check if a player can win next turn\nPress 'q' to quit preset game\nPress Enter to continue""")
                player_next_turn_input = input("Choose option: ")
                # If players choose 'y' checks if the players has an option to win in the next turn if so prints it
                if player_next_turn_input.lower() == 'y':
                    if check_winner(game_board, 'X', True):
                        print(f'Player X next win move: {check_winner(game_board, "X", True)}')
                    else:
                        print(f'Player X has no next win moves')
                    if check_winner(game_board, 'O', True):
                        print(f'Player O next win move: {check_winner(game_board, "O", True)}')
                    else:
                        print(f'Player O has no next win moves')
                # If players choose 'q' ending the current game and going back to the main menu
                elif player_next_turn_input.lower() == 'q':
                    print(f'Quiting current game restarting game board to default size')
                    break
            
            # If the game ended gives the players the following options
            if game_ended:
                counter = 0
                turn = 0
                # Removing the all the players choices from the game board
                game_board = set_board_size(len(game_board))
                player_input = input("Press 'q' to return to main menu or Enter to continue: ")
                # If players choose 'q' ends the current games session and going to the main menu if any other restarts the game
                if player_input.lower() == 'q':
                    print('Restarting game board to default size')
                    game_board = set_board_size(3)
                    break

# Running main function to start the program
main()
