import random

# Question 1:
print('Question 1:')
with open('game_treasure.txt','w+') as f:
    treasure_word = 'TREASURE'
    game_treasure = ''
    list_of_nums = list(range(10)) + list(reversed(range(10)))
    
    for num in list_of_nums:
        if num == 9 and game_treasure.count('9') > 0:
            game_treasure += treasure_word
        repeat_num = random.randint(1,20)
        for temp_num in range(repeat_num):
            game_treasure += str(num)

    f.write(game_treasure)
    f.seek(0)
    game_treasure = list(str(f.readlines()).replace("['","").replace("']",""))
    current_location = 0
    treasure_word = list(treasure_word)
    turns = 0

    while True:
        direction = input('Where you want to move? [1-forward 2-backwards] ')
        if direction != '1' and direction != '2':
            print('Error: Wrong direction input was chosen')
            continue
        moves = input('How many characters? ')
        if not moves.isdigit():
            print('Error: No eligable number if characters were chosen')
            continue
        else:
            moves = int(moves)

        if direction == '1':
            if current_location + moves >= len(game_treasure):
                print('Error: Out of bound of characters in game')
                continue
            current_location += moves

        elif direction == '2':
            if current_location - moves <= 0:
                print('Error: Out of bound of characters in game')
                continue
            current_location -= moves

        turns += 1
        player_location_value = game_treasure[current_location]
        if player_location_value in treasure_word:
            print(f'You hit "{player_location_value}"')
            print('Congratulations, You found the TREASURE!!!')
            print(f'The number of turns to found the TREASURE: {turns}')
            break
        else:
            print(f'You hit "{player_location_value}"')
            print('… again … until hit one of the "TREASURE"”" letters…')

# Question 2:
print('Question 2:')
# Creating a default file for the question
with open('One_file.txt','w') as f:
    f.writelines('Hello\nWorld\nThis\nIs\nPython\n')

def GetFileSize(file_name):
    with open(file_name,'r') as f:
        f.seek(0,2)
        return f.tell()
print(f'File size: {GetFileSize("One_file.txt")}')

# Question 3:
print('Question 3:')
# Using function from question 2
# Creating default files for the question
file_list = []
for num in range(1,4):
    with open(f'File_{num}.txt','w') as f:
        f.writelines(f'{random.randint(1,1000)}\n{random.randint(1,1000)}\n{random.randint(1,1000)}\n')
    file_list.append(f'File_{num}.txt')

def GetAllSizes(file_names_list):
    file_names_dict = {}
    for file_name in file_names_list:
        file_names_dict[file_name] = GetFileSize(file_name)
    return file_names_dict
print(f'File sizes: {GetAllSizes(file_list)}')

# Question 4:
# Using function from question 3
print('Question 4:')
def GetSumSize(file_names_list):
    file_sizes = sum(GetAllSizes(file_names_list).values())
    return file_sizes
print(f'The sum of all files: {GetSumSize(file_list)}')

# Question 5:
print('Question 5:')
# Creating a default file for the question
with open('Words_file.txt','w') as f:
    f.writelines('Hello\nWorld\nIn\nWorld\nThis\nIs\nPython\nFrom\nPython\n')

def GetWordsFromFile(file_name):
    with open(file_name,'r') as f:
        words = list(set([word.strip() for word in f.readlines()]))
        return words
print(f'Unique words from file: {GetWordsFromFile("Words_file.txt")}')

# Question 6:
print('Question 6:')
# Using file from question 5

def GetWordsCountFromFile(file_name):
    with open(file_name, 'r') as f:
        words = [word.strip() for word in f.readlines()]
        words_dict = dict()
        for word in words:
            if not word in words_dict.keys():
                words_dict[word] = words.count(word)
        return words_dict
print(GetWordsCountFromFile('Words_file.txt'))


# Question 7:
print('Question 7:')
# Creating a default file for the question
with open('Input_file.txt','w') as f:
    f.writelines('Hello\nWorld\nIn\nWorld\nThis\nIs\nPython\nFrom\nPython')

def WriteReverse(input_file, output_file):
    with open(input_file, 'r') as sf, open(output_file, 'w') as df:
        input_content = [word.strip() for word in sf.readlines()]
        output_content = [word[::-1] for word in input_content[::-1]]
        print(f'Writing to a file the following: {output_content}')
        df.write('\n'.join(output_content))
WriteReverse('Input_file.txt','Output_file.txt')

# Question 8:
print('Question 8:')
# Using file from question 5

def FindWord(file_name,word):
    with open(file_name,'r') as f:
        list_of_words = [word.strip() for word in f.readlines()]
        if word in list_of_words:
            return True
        else:
            return False
print(FindWord('Words_file.txt','Hello'))
print(FindWord('Words_file.txt','Python'))
print(FindWord('Words_file.txt','NoWord'))

# Question 9:
print('Question 9:')
# Using function from question 8 and file from question 5

def AddWordIfNotExist(file_name,word):
    if FindWord(file_name,word):
        print(f'Word {word} already in file')
    else:
        print(f'Adding word {word} to file')
        with open(file_name,'a') as f:
            f.write(f'{word}\n')
AddWordIfNotExist('Words_file.txt','Python')
AddWordIfNotExist('Words_file.txt','NewWord')